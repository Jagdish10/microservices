from app.api.models import *
from app.api.db import casts,database

async def add_casts(payload: CastIn):
    query = casts.insert().values(**payload.dict())
    return await database.execute(query = query)

async def get_all_casts():
    query = casts.select()
    return await database.fetch_all(query = query)
async def get_cast_by_id(id: int):
    query = casts.select(casts.c.id == id)
    return await database.fetch_one(query = query)

async def update_cast(id: int ,payload: CastIn):
    query = casts.update().where(casts.c.id == id).values(**payload.dict())
    return await database.execute( query = query)